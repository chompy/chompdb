'''
  ChompDB by Nathan Ogden - A Python Sqlite/MySql ORM
  =======================================================================
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
  
import sqlite3, time
try: 
  import mysql.connector
except ImportError:
  pass
    
class ChompDB():

  '''Main database class. Opens SQLITE file.'''

  def __init__(self, dbtype="sqlite", dbconnect = "database.db", username=None, password=None, database = None):
  
    '''Connects to database.
    
    Keyword arguments:
    dbtype string -- type of database (sqlite, mysql)
    dbconnect string -- for sqlite path to database file, for mysql host to connect to
    username string -- username for database
    password string -- password for database
    database string -- [Mysql Only] Database to use
    
    Returns: None
    '''    
  
    self.dbtype = dbtype
  
    # SQLITE
    if dbtype == "sqlite":
      self.db = sqlite3.connect(dbconnect)
      
    # MYSQL
    if dbtype == "mysql":
      try:
        self.db = mysql.connector.connect(host = dbconnect, user = username, password = password, database = database)
      except:
        return None
      
      #self.db.set_character_set('utf8')
      
      cur = self.db.cursor()
      cur.execute('SET NAMES utf8;')
      cur.execute('SET CHARACTER SET utf8;')
      cur.execute('SET character_set_connection=utf8;')
      self.db.commit()
      cur.close()
      
    self.objs = []                                

  def dispense(self, table):
  
    '''Creates a new empty ChompDBObject.
    
    Keyword arguments:
    table string -- name of DB table to use
    
    Returns: ChompDBObject
    '''  

    cur = self.db.cursor()
    cur.execute("show tables like '" + table + "'")
    count = cur.fetchone()
    
    if count <= 0:
      if self.dbtype == "sqlite":
        self.db.query("CREATE TABLE IF NOT EXISTS " + table + "(id INTEGER PRIMARY KEY)")
        self.db.commit()

      elif self.dbtype == "mysql":
        cur2 = self.db.cursor()
        cur2.execute("CREATE TABLE IF NOT EXISTS " + table + "(id INTEGER AUTO_INCREMENT PRIMARY KEY)")
        self.db.commit()
        cur2.close()
      
    cur.close()          
    return ChompDBObject(self.db, self.dbtype, table)
    
  def load(self, table, id): 

    '''Returns a single ChompDBObject from table row with
       given ID.
    
    Keyword arguments:
    table string -- name of DB table to use
    id int -- ID of row to pulll
    
    Returns: ChompDBObject
    '''    
  
    try:    
      cur = self.db.cursor()
      cur.execute("SELECT * FROM " + table + " WHERE id = " + str(int(id)) + "")
      vals = cur.fetchone()
      cur.close()
      if vals:
        return ChompDBObject(self.db, self.dbtype, table, vals)
      else: return None
    except sqlite3.OperationalError: 
      cur.close()
      return False
    except mysql.connector.Error as err:
      cur.close()
      return False
        

    
  def find(self, table, query = "1=1", vars = []):

    '''Returns array of ChompDBObjects that match
       given query.
    
    Keyword arguments:
    table string -- name of DB table to use
    query string -- query that goes after WHERE clause in sql query
    vars array -- array of variables to inject into query (escapes them)
    
    Returns: array
    '''     
  
    cur = self.db.cursor()

    try:
      if self.dbtype == "mysql": query = query.replace("?", "%s")
      cur.execute("SELECT * FROM " + table + " WHERE " + query, vars)
      vals = cur.fetchall()
      results = []
  
      for i in vals:
        results.append(ChompDBObject(self.db, self.dbtype, table, i))
      cur.close()
      return results

    except sqlite3.OperationalError: 
      cur.close()
      return []        
    except mysql.connector.Error as err:
      cur.close()
      return [] 
        
  def findOne(self, table, query = "1=1", vars = []):
  
    ''' Sams as ChompDB.find except only returns the first result. '''
    res = self.find(table, query, vars)
    if not res: return None
    else: return res[0]
    
  def findWith(self, table, array):

    '''Returns array of ChmopDBObjects whose columns match
       values found in given array
    
    Keyword arguments:
    table string -- name of DB table to use
    array dict -- key/value pairs that identify columns and values in sql table
    
    Returns: array
    '''      
  
    try:
      sql_str = "SELECT * FROM " + table + " WHERE"
      for i in array:
        
        sql_str += " " + i + " = '" + str(array[i]) + "' AND"
        
      sql_str = sql_str[:-3]
      cur = self.db.cursor()
      vals = cur.execute(sql_str)
      results = []
      for i in vals:
  
        results.append(ChompDBObject(self.db, self.dbtype, table, i))
                 
      cur.close()        
      return results

    except sqlite3.OperationalError:
      cur.close()
      return []
  
  def count(self, table, query="1=1", vars = []):
  
    '''Returns number of rows in given table.
    
    Keyword arguments:
    table string -- name of DB table to use
    query string -- query that goes after WHERE clause in sql query
    vars array -- array of variables to inject into query (escapes them)
    
    Returns: int
    '''    
    
    cur = self.db.cursor()
    if self.dbtype == "mysql": query = query.replace("?", "%s")    
    cur.execute("SELECT COUNT(*) from " + table + " WHERE " + query, vars)
    result = cur.fetchone()
    cur.close()
    
    return result[0]
    

  def query(self, query, vars = []):
  
    '''Perform a raw query.
    
    Keyword arguments:
    query string -- query that goes after WHERE clause in sql query
    vars array -- array of variables to inject into query (escapes them)
    
    Returns db.cursor object
    '''      
    
    cur = self.db.cursor()
    if self.dbtype == "mysql": query = query.replace("?", "%s")    
    cur.execute(query, vars)
    self.db.commit()
    return cur
    
  def store(self, rows):
    
    '''Stores multiple table rows at once.
    
    Keywork arguments:
    rows array -- array of ChompDBObjects to store
    
    Returns bool
    '''
    
    # make a cursor
    cursor = self.db.cursor()
    
    # iterate each ChompDBObject
    for row in rows:
      if not row: continue
      row.store(cursor)
    self.db.commit()
    cursor.close()
    return True
        
class ChompDBObject():

  '''DB Object class, each instance of this class
  is a row in a DB table.'''

  def __init__(self, db, dbtype, table, values = None):
    self.dbtype = dbtype
    cur = db.cursor() 
    
    # get list of fields
    if dbtype == "sqlite":                                     
      cur.execute("PRAGMA table_info(" + table + ");")
    elif dbtype == "mysql":
      cur.execute("DESC " + table + ";")  
      
    self.values = {}
    ct = 0
    
    # field names pop up in different places in array depending on
    # dbtype
    fieldGetName = 1
    if dbtype == "mysql": fieldGetName = 0
    
    # get list of fields
    for i in cur.fetchall():
    
      # set values
      if values:
        self.values[i[fieldGetName]] = values[ct]
      else:
        if not i[1] == "id": 
          if self.dbtype == "sqlite":
            self.values[i[fieldGetName]] = ""
          elif self.dbtype == "mysql":
            self.values[i[fieldGetName]] = i[4]
        else: self.values[i[fieldGetName]] = None
      ct += 1
    
    self.table = table
    self.db = db
    cur.close()

  def set(self, key, val):

    '''Sets a value of item in DB row
    
    Keyword arguments:
    key string -- name of DB column to set value to
    val string -- value to set
    
    Returns: boolean
    '''      
  
    hasCol = 0
    for i in self.values:
      if i == key:
        hasCol = 1
        break

    # set var and escape
    self.values[key] = val
    
    # get/set field type (mysql only)
    varType = "TEXT"
    if type(self.values[key]) is int:
      varType = "BIGINT(255)"
    elif type(self.values[key]) is float:
      varType = "DOUBLE"
    elif type(self.values[key]) is bool:
      varType = "BOOLEAN"

    if not hasCol:
      if self.dbtype == "sqlite":
        self.db.execute("ALTER TABLE `" + self.table + "` ADD `" + key + "` VARCHAR")
        self.db.commit()
      elif self.dbtype == "mysql":
        cur = self.db.cursor() 
        if varType == "TEXT":
          cur.execute("ALTER TABLE `" + self.table + "` ADD `" + key + "` " + varType + " CHARACTER SET utf8 COLLATE utf8_general_ci")         
        else:
          cur.execute("ALTER TABLE `" + self.table + "` ADD `" + key + "` " + varType) 
        self.db.commit()
        cur.close()     
          
    return True

  def store(self, cursor = None):


    '''Stores all values to database
    
    Keyword arguments:
    cursor object -- optional provided cursor(for making a lot of queries)
    
    Returns: Table row ID
    '''        
  
    if len(self.values) <= 0: return 0  
    
    val_replace = "?"
    if self.dbtype == "mysql": val_replace = "%s"
    
    table_vals = []
    if not self.values['id']:
      sql_str = "INSERT INTO `" + self.table + "` ("

      for i in self.values:
        if not i == "id":
          #table_vals.append(i)
          sql_str += "`" + i + "`,"
      sql_str = sql_str[:-1]

      sql_str += ") VALUES ("

      for i in self.values:
        if not i == "id":
          table_vals.append(self.values[i])
          sql_str += val_replace + ","
      sql_str = sql_str[:-1]
      sql_str += ")"

    else:
      sql_str = "UPDATE `" + self.table + "` SET "
      for i in self.values:
        if not i == "id":
          table_vals.append(self.values[i])
          sql_str += "`" + i + "` = " + val_replace + ","
          
      sql_str = sql_str[:-1]
      table_vals.append(self.values['id'])
      sql_str += " WHERE id = " + val_replace

    if not cursor:
      cursor = self.db.cursor() 
    else:
      cursor.execute(sql_str, table_vals)
      row_id = cursor.lastrowid
      return row_id
    
    try:
      cursor.execute(sql_str, table_vals)
      row_id = cursor.lastrowid
      self.db.commit()
    except Exception, e:
      if 'Deadlock' in e:
        time.sleep(1)
        cur.execute(sql_str, table_vals)
        row_id = cursor.lastrowid
        self.db.commit()   
    cursor.close()    
    return row_id

  def get(self, key):

    '''Gets a value from table row
    
    Keyword arguments:
    key string -- name of DB column to get from
    
    Returns: string
    '''        
  
    try:
      return self.values[key]
    except KeyError:
      return None
      
  def getFieldNames(self):
  
    '''Retrives a list of columns names
    
    Returns: array
    '''        
  
    fields = []
    for i in self.values:
      fields.append(i)
    return fields

  def export(self):
  
    '''Returns an array of all values in this row as
    key/value pairs.
    
    Returns: array
    '''
  
    arr = {}
    for field in self.getFieldNames():
      arr[field] = self.get(field)
      
    return arr

  def trash(self):

    '''Delete this row from the table
    
    Returns: boolean
    '''     
    
    if self.dbtype == "sqlite":
      self.db.execute("DELETE FROM `" + self.table + "` WHERE `id` = " + str(self.values['id']))
      self.db.commit()
    elif self.dbtype == "mysql":
      cur = self.db.cursor()
      cur.execute("DELETE FROM `" + self.table + "` WHERE `id` = " + str(self.values['id']))    
      self.db.commit()
      cur.close()
    
    del self
    return True
